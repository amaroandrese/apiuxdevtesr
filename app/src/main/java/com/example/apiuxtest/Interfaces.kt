package com.example.apiuxtest

import android.content.Context
import com.example.apiuxtest.View.Adapter.ExchangeData

public interface Interfaces{

    /**
     * Interfaces de la View
     * */

    //Operaciones disponibles de la vista para comunicación desde el Presenter
    interface RequiredViewMethods{
        fun getAppContext(): Context
        fun getActivityContext() : Context
        fun notifyExchangeChanged(exchangeList: ArrayList<ExchangeData>)

    }

    /**
     * Interfaces del Presenter
     * */

    //Operaciones ofrecidas a la View para comunicación con el Presenter
    interface ProvidedPresenterMethods{
        fun updateValuesAccordingToExchange(exchange:String)
    }

    //Operaciones requeridas por el Model para comunicación con Presenter
    interface RequiredPresenterMethods{
        fun getAppContext():Context
        fun getActivityContext():Context
    }

    /**
     * Interfaces del Model
     *
     * */

    //Operaciones disponibles del Model para comunicación con el Presenter
    interface ProvidedModelMethods{

        fun getExchangeValue():Int
        fun insertExchangeValues():Boolean
        fun getData(exchangeType:String,onFinishedListener: OnFinishedListener)

    }

    interface OnFinishedListener {
        fun onResultSuccess(arrUpdates: ArrayList<ExchangeData>)
        fun onResultFail(strError: String)
    }


}