package com.example.apiuxtest.Model

import com.example.apiuxtest.Interfaces
import com.example.apiuxtest.View.Adapter.ExchangeData
import com.example.apiuxtest.Volley.OperationCallback
import com.example.apiuxtest.Volley.VolleyImplementation

class ExchangeModel(var mPresenter: Interfaces.RequiredPresenterMethods) : Interfaces.ProvidedModelMethods {

    override fun getExchangeValue(): Int {
        TODO("Not yet implemented")
    }

    override fun insertExchangeValues(): Boolean {
        TODO("Not yet implemented")
    }

    override fun getData(exchangeType:String, onFinishedListener : Interfaces.OnFinishedListener){
        VolleyImplementation.getInstance(mPresenter.getAppContext()).getAllData(exchangeType,object:
            OperationCallback {
            override fun onSuccess(mList : ArrayList<ExchangeData>) {
                onFinishedListener.onResultSuccess(mList)
            }
            override fun onError(obj: Any?) {
                onFinishedListener.onResultFail(obj.toString())
            }
        })
    }




}