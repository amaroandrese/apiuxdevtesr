package com.example.apiuxtest.Presenter

import android.content.Context
import android.util.Log
import com.example.apiuxtest.Interfaces
import com.example.apiuxtest.View.Adapter.ExchangeData
import java.lang.ref.WeakReference
import java.net.Inet4Address

class Presenter: Interfaces.ProvidedPresenterMethods,Interfaces.RequiredPresenterMethods,
    Interfaces.OnFinishedListener {

    var mView : WeakReference<Interfaces.RequiredViewMethods>
    lateinit var mModel : Interfaces.ProvidedModelMethods


    constructor(mView: Interfaces.RequiredViewMethods) {
        this.mView = WeakReference(mView)
    }

    private fun getView() : Interfaces.RequiredViewMethods? {
        return mView.get()
    }

    fun setModel(mModel: Interfaces.ProvidedModelMethods){
        this.mModel = mModel
    }

    override fun getAppContext(): Context {
        return getView()!!.getAppContext()
    }

    override fun getActivityContext(): Context {
        return getView()!!.getActivityContext()
    }

    override fun updateValuesAccordingToExchange(exchange: String) {
        mModel.getData(exchange,this)
    }

    override fun onResultSuccess(arrUpdates: ArrayList<ExchangeData>) {
        getView()!!.notifyExchangeChanged(arrUpdates)
    }

    override fun onResultFail(strError: String) {
        Log.e("onResultFail","error calling api-------------")
    }
}