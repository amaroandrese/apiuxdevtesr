package com.example.apiuxtest.View.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apiuxtest.R


class ExchangeAdapter(val items: List<ExchangeData>) : RecyclerView.Adapter<ExchangeViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExchangeViewHolder {

        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.exchangecardview,parent,false)
        return  ExchangeViewHolder(view)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ExchangeViewHolder, position: Int) = holder.bind(items[position])



}

