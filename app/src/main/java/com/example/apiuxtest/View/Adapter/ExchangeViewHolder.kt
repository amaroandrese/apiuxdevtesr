package com.example.apiuxtest.View.Adapter

import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.apiuxtest.R
import kotlinx.android.synthetic.main.exchangecardview.view.*
import java.math.RoundingMode
import java.text.DecimalFormat

class ExchangeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(exchangeData: ExchangeData) = with(itemView) {
        //TODO: bindeamos la info
        val df = DecimalFormat("#.####")
        df.roundingMode = RoundingMode.CEILING
        itemView.CV_exchangeValue.text = exchangeData.exchangeName+": "+df.format(exchangeData.exchangeValue)
        if(exchangeData.exchangeName.equals("GBP"))
            itemView.CV_imageview.setImageDrawable(ContextCompat.getDrawable(itemView.context,R.drawable.united_kingdom_flag))
        else if(exchangeData.exchangeName.equals("MXN"))
            itemView.CV_imageview.setImageDrawable(ContextCompat.getDrawable(itemView.context,R.drawable.mexico_flag))
        else if(exchangeData.exchangeName.equals("CAD"))
            itemView.CV_imageview.setImageDrawable(ContextCompat.getDrawable(itemView.context,R.drawable.canada_flag))

    }

}