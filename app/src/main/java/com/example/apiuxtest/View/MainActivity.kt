package com.example.apiuxtest.View

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apiuxtest.Interfaces
import com.example.apiuxtest.Model.ExchangeModel
import com.example.apiuxtest.Presenter.Presenter
import com.example.apiuxtest.R
import com.example.apiuxtest.View.Adapter.ExchangeAdapter
import com.example.apiuxtest.View.Adapter.ExchangeData


class MainActivity : AppCompatActivity(), Interfaces.RequiredViewMethods,SensorEventListener {

    lateinit var mPresenter : Interfaces.ProvidedPresenterMethods
    lateinit var mListExchange: ArrayList<ExchangeData>
    lateinit var adapter: ExchangeAdapter
    lateinit var gyroImageView : ImageView


    private var mSensorManager: SensorManager? = null
    private var mSensorGyr: Sensor? = null
    private val ROTATION_THRESHOLD_LEFT = -0.3f
    private val ROTATION_THRESHOLD_RIGHT = 0.3f
    private val ROTATION_WAIT_TIME_MS = 100
    private var mRotationTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            this.supportActionBar!!.hide()
        } catch (e: NullPointerException) {
        }
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)
        val rvExchangeData = findViewById<View>(R.id.recycler_view) as RecyclerView
        mListExchange = ArrayList<ExchangeData>()
        adapter = ExchangeAdapter(mListExchange)
        rvExchangeData.adapter = adapter
        rvExchangeData.layoutManager = LinearLayoutManager(this)
        setupMVP()
        setupGyro()


    }

    override fun onResume() {
        super.onResume()
        mSensorManager!!.registerListener(this, mSensorGyr, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        mSensorManager!!.unregisterListener(this)
    }


    private fun setupGyro(){

        gyroImageView = findViewById(R.id.IV_gyroscope)
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mSensorGyr = mSensorManager!!.getDefaultSensor(Sensor.TYPE_GYROSCOPE)

    }

    private fun setupMVP(){

        // Create the Presenter
        val presenter = Presenter(this)
        // Create the Model
        val model = ExchangeModel(presenter)
        // Set Presenter model
        presenter.setModel(model)
        // Set the Presenter as a interface
        mPresenter = presenter

    }

    fun onRadioButtonClicked(view: View) {
        if(view is RadioButton){
            val checked = view.isChecked

            when(view.getId()){
                R.id.exchange_EUR -> if(checked){
                    mPresenter.updateValuesAccordingToExchange("EUR")
                }
                R.id.exchange_USD -> if(checked){
                    mPresenter.updateValuesAccordingToExchange("USD")
                }
            }
        }
    }

    override fun getAppContext(): Context {
        return applicationContext
    }

    override fun getActivityContext(): Context {
        return this
    }

    override fun notifyExchangeChanged(exchangeList : ArrayList<ExchangeData>) {

        mListExchange.clear()
        mListExchange.addAll(exchangeList)
        adapter.notifyDataSetChanged()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            if (event.sensor.type == Sensor.TYPE_GYROSCOPE) {
                Toast.makeText(this,"ERROR EN EL GIROSCOPIO",Toast.LENGTH_SHORT).show()
            }
            return
        }
        if (event.sensor.type == Sensor.TYPE_GYROSCOPE) {
            detectRotation(event)
        }
    }

    private fun detectRotation(event: SensorEvent) {
        val now = System.currentTimeMillis()
        if (now - mRotationTime > ROTATION_WAIT_TIME_MS) {
            mRotationTime = now
            //cambiamos la imagen
            if(event.values[2] < ROTATION_THRESHOLD_LEFT){
                gyroImageView.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.rightarrowimage))
            }else if(event.values[2] > ROTATION_THRESHOLD_RIGHT){
                gyroImageView.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.leftarrowimage))
            }

        }
    }
}
