package com.example.apiuxtest.Volley

import com.example.apiuxtest.View.Adapter.ExchangeData

interface OperationCallback {

    fun onSuccess(mList:ArrayList<ExchangeData>)
    fun onError(obj:Any?)
}