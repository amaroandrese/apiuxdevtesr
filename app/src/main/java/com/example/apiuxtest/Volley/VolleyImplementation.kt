package com.example.apiuxtest.Volley

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.apiuxtest.View.Adapter.ExchangeData
import org.json.JSONArray
import org.json.JSONObject


class VolleyImplementation(val context: Context) {

    val TAG = "VolleyImplementation"

    private val requestQueue = Volley.newRequestQueue(context)
    private val url = "https://api.exchangeratesapi.io/latest?base="

    fun getAllData(exchangeValue: String,callback: OperationCallback){

        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET,url+exchangeValue,JSONObject(),

            Response.Listener { response ->

                var mList = ArrayList<ExchangeData>()
                var exchangeData : ExchangeData
                var jsonObject = JSONObject(response.toString())
                var rates = jsonObject.get("rates") as JSONObject
                val key: JSONArray = rates.names()

                for (i in 0 until key.length()) {
                    val mKey = key.getString(i)
                    val value: Double = rates.getDouble(mKey)
                    exchangeData = ExchangeData(mKey, value.toFloat())
                    if(mKey == "CAD" || mKey == "GBP" || mKey == "MXN"){
                        mList.add(exchangeData)
                    }

                }
                callback.onSuccess(mList)

            },Response.ErrorListener { error ->
                val messageError= "error : ${error.networkResponse.statusCode} ".plus("message ${error.message}")
                callback.onError(messageError)
            })
        requestQueue.add(jsonObjectRequest)
    }

    fun cancelOperation(){
        requestQueue?.cancelAll(TAG)
    }

    companion object {
        private var INSTANCE: VolleyImplementation? = null
        @JvmStatic fun getInstance(context: Context): VolleyImplementation {
            return INSTANCE ?: VolleyImplementation(context)
                .apply { INSTANCE = this }
        }
        @JvmStatic fun destroyInstance() {
            INSTANCE = null
        }
    }
}